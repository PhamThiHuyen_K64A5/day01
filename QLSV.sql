drop database if exists QLSV; 

create database QLSV;

use QLSV;

create table DMKHOA(
    MaKH varchar(6) primary key,
    TenKhoa varchar(30)
);

create table SINHVIEN(
    MaSV varchar(6) primary key,
    HoSV varchar(30),
    TenSV varchar(15),
    GioiTinh char(1),
    NgaySinh datetime,
    NoiSinh varchar(50),
    DiaChi varchar(50),
    MaKH varchar(6),
    HocBong int
);

INSERT INTO `dmkhoa` (`MaKH`, `TenKhoa`) VALUES ('1', 'Công nghệ thông tin');
INSERT INTO `dmkhoa` (`MaKH`, `TenKhoa`) VALUES ('2', 'Toán');

INSERT INTO `sinhvien` (`MaSV`, `HoSV`, `TenSV`, `GioiTinh`, `NgaySinh`, `NoiSinh`, `DiaChi`, `MaKH`, `HocBong`) VALUES ('001', 'Nguyễn', 'A', '1', '2013-09-04 15:42:35', '67 PK, NTL, HN', '67 PK, NTL, HN', '1', '0');
INSERT INTO `sinhvien` (`MaSV`, `HoSV`, `TenSV`, `GioiTinh`, `NgaySinh`, `NoiSinh`, `DiaChi`, `MaKH`, `HocBong`) VALUES ('002', 'Trần', 'B', '1', '2013-09-04 15:42:35', '67 PK, NTL, HN', '67 PK, NTL, HN', '2', '1');

